https://thedevelopercafe.com/articles/sql-in-go-with-sqlboiler-ac8efc4c5cb8#eager-loading-articles


go mod edit -go 1.20

go mod edit -module {NEW_MODULE_NAME}
-- rename all imported module
find . -type f -name '*.go' \
  -exec sed -i -e 's,{OLD_MODULE},{NEW_MODULE},g' {} \;

go mod edit -module gitlab.com/remotejob/go-sql-boiler-example

find . -type f -name '*.go' -exec sed -i -e 's,github.com/gurleensethi/go-sql-boiler-example,gitlab.com/remotejob/go-sql-boiler-example,g' {} \;

docker-compose up --build -d postgres

sqlboiler psql 

go get -u ./...